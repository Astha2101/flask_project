from flask import Flask, render_template, url_for, request, redirect, jsonify,Response
from flask.scaffold import F
from flask_serialize.flask_serialize import FlaskSerialize
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from flask_serialize import FlaskSerializeMixin

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)

fs_mixin=FlaskSerialize(db)

class Todo(fs_mixin,db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(200), nullable=False)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
     
    create_fields=update_fields=['content','date_created']

    def __repr__(self):
        return '<Task %r>' % self.id


@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        task_content = request.form['content']
        new_task = Todo(content=task_content)

        try:
            db.session.add(new_task)
            db.session.commit()
            return redirect('/')
        except:
            return 'There was an issue adding your task'

    else:
        tasks = Todo.query.order_by(Todo.date_created).all()
        return render_template('index.html', tasks=tasks)


@app.route('/api/create',methods=['POST'])
def create():
    return Todo.get_delete_put_post()


@app.route('/api', methods=['GET'])
def getall_tasks():
    if request.method=='GET':
        return Todo.get_delete_put_post()

@app.route('/api/<int:id>', methods=['GET'])
def get(id):
    if request.method=='GET':
        return Todo.get_delete_put_post(id)


@app.route('/delete/<int:id>')
def delete(id):
    task_to_delete = Todo.query.get_or_404(id)

    try:
        db.session.delete(task_to_delete)
        db.session.commit()
        return redirect('/')
    except:
        return 'There was a problem deleting that task'

@app.route('/api/delete/<int:id>',methods=['DELETE'])
def api_delete(id):
    return Todo.get_delete_put_post(id)

@app.route('/update/<int:id>', methods=['GET', 'POST'])
def update(id):
    task = Todo.query.get_or_404(id)

    if request.method == 'POST':
        task.content = request.form['content']

        try:
            db.session.commit()
            return redirect('/')
        except:
            return 'There was an issue updating your task'

    else:
        return render_template('update.html', task=task)

@app.route('/api/update/<int:id>',methods=['POST','GET'])
def api_update(id):
    return Todo.get_delete_put_post(id)

if __name__ == "__main__":
    app.run(debug=True)
