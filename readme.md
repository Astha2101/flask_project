# Task-Manager
A no frills task manager that's really intuitive and simple to use and kepp up with your daily tasks.

## Index
* UI
   * Add Tasks
   * View Tasks
   * Update Tasks
   * Delete Tasks 

<br>
### Endpoints :

- `GET` **/api** - To get all the tasks
- `GET` **/api/id** - To get a task by passing the task id.
- `POST` **/api/create** - To create task. Takes `{"content"}` in json body. 
- `DELETE` **/api/delete/id>** - To delete the task, takes id in url.
- `POST` **/api/update/id** - To update a task. Takes `{"content"}` in json body. 

<br>



## Installation
First of all you have to prepare your environment. Select
a location where you want to store the files. I will use 
Projects as my example. 

    mkdir Projects
    cd Projects
    git clone git clone https://Astha2101@bitbucket.org/Astha2101/flask_project.git
    python3 -m venv venv
    source venv/bin/activate
    pip install -r requirements.txt
    python app.py

Then simply open up a browser, Chrome/Chromium recommended,
to [localhost:5000](http://localhost:5000/) and play around
with it :).